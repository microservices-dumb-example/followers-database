# followers database
# Copyright (C) 2020 Bruno Mondelo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

FROM debezium/postgres:12-alpine

COPY . /database

WORKDIR /database
RUN /bin/sh dbdumper/dbdumper.sh -s develop -d followers -c > /docker-entrypoint-initdb.d/followers_develop.sql
RUN /bin/sh dbdumper/dbdumper.sh -s production -d followers > /docker-entrypoint-initdb.d/followers_production.sql
RUN /bin/sh dbdumper/dbdumper.sh -s staging -d followers > /docker-entrypoint-initdb.d/followers_staging.sql

WORKDIR /

CMD ["postgres"]
