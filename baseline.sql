/*
   baseline.sql database baseline
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE SCHEMA {{schema}};

CREATE TABLE {{schema}}.schema_version (
    id            SERIAL PRIMARY KEY,
    major_version INTEGER NOT NULL,
    minor_version INTEGER NOT NULL,
    build_version INTEGER NOT NULL,
    name          VARCHAR NOT NULL,
    applied_at    TIMESTAMP WITH TIME ZONE
        DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL,

    UNIQUE (major_version, minor_version, build_version)
);

CREATE TABLE {{schema}}.users (
    id         BIGSERIAL PRIMARY KEY,
    user_uuid  UUID DEFAULT uuid_generate_v1mc() NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    added_at   TIMESTAMP WITH TIME ZONE
        DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL,

    UNIQUE(user_uuid)
);

CREATE TABLE {{schema}}.followers (
    follower    BIGINT NOT NULL,
    following   BIGINT NOT NULL,
    followed_at TIMESTAMP WITH TIME ZONE
        DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL,

    PRIMARY KEY(follower, following),
    FOREIGN KEY(follower) REFERENCES {{schema}}.users(id) ON DELETE RESTRICT
        ON UPDATE RESTRICT,
    FOREIGN KEY(following) REFERENCES {{schema}}.users(id) ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

INSERT INTO {{schema}}.schema_version VALUES(
    DEFAULT,
    1,
    0,
    0,
    'Baseline',
    DEFAULT
);
